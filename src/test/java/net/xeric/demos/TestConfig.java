package net.xeric.demos;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;

/**
 * This is the Spring configuration file that allows us to get the webdriver.
 */
@Configuration
public class TestConfig {

    private static final String MAC_CHROME_DRIVER = "./src/test/resources/bin/chromedriver";
    private static final String WINDOWS_CHROME_DRIVER = "./src/test/resources/bin/chromedriver.exe";
    private static final String MAC_GECKO_DRIVER = "./src/test/resources/bin/geckodriver-mac";
    private static final String LINUX_GECKO_DRIVER = "./src/test/resources/bin/geckodriver-linux";

    @Bean(destroyMethod = "quit")
    @Lazy
    @Scope("singleton")
    public WebDriver getWebDriver() {
//        String driverPath = isWindows()? WINDOWS_CHROME_DRIVER : MAC_CHROME_DRIVER;
//        System.setProperty("webdriver.chrome.driver", driverPath);
//        return new ChromeDriver();

        FirefoxOptions firefoxOptions = new FirefoxOptions();
        System.setProperty("webdriver.gecko.driver", "./src/test/resources/bin/geckodriver-mac");

        if(isLinux()) {
          System.setProperty("webdriver.gecko.driver", "./src/test/resources/bin/geckodriver-linux");
          firefoxOptions.setHeadless(true);
      }
        return new FirefoxDriver(firefoxOptions);

    }

    private boolean isWindows() {
        return System.getProperty("os.name").toLowerCase().contains("win");
    }

    private boolean isLinux() {
        return System.getProperty("os.name").toLowerCase().contains("linux");
    }
    private boolean isMac() {
        return System.getProperty("os.name").toLowerCase().contains("mac");
    }

}
