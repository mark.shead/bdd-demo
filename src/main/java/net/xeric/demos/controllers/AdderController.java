package net.xeric.demos.controllers;

import net.xeric.demos.services.AdderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by markshead on 3/30/16.
 */
@RestController
public class AdderController {

    @Autowired
    private AdderService adderService;

    @GetMapping("/adder")
    public int adder(@RequestParam(value = "firstNumber", defaultValue = "0") int firstNumber, @RequestParam(value = "secondNumber", defaultValue = "0") int secondNumber) {
        return adderService.add(firstNumber, secondNumber);
    }


}
